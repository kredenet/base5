<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200626105317 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB3642F50D82F4');
        $this->addSql('DROP TABLE form_field');
        $this->addSql('DROP INDEX IDX_C8CB3642F50D82F4 ON reg_value');
        $this->addSql('ALTER TABLE reg_value CHANGE form_field_id form_group_id INT NOT NULL');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB36424F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id)');
        $this->addSql('CREATE INDEX IDX_C8CB36424F6E5C03 ON reg_value (form_group_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE form_field (id INT AUTO_INCREMENT NOT NULL, form_group_id INT NOT NULL, name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, type VARCHAR(20) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D8B2E19B4F6E5C03 (form_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE form_field ADD CONSTRAINT FK_D8B2E19B4F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB36424F6E5C03');
        $this->addSql('DROP INDEX IDX_C8CB36424F6E5C03 ON reg_value');
        $this->addSql('ALTER TABLE reg_value CHANGE form_group_id form_field_id INT NOT NULL');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB3642F50D82F4 FOREIGN KEY (form_field_id) REFERENCES form_field (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C8CB3642F50D82F4 ON reg_value (form_field_id)');
    }
}
