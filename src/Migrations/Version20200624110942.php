<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200624110942 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE form_field (id INT AUTO_INCREMENT NOT NULL, form_group_id INT NOT NULL, name VARCHAR(100) NOT NULL, type VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D8B2E19B4F6E5C03 (form_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE form_group (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EE87E00612469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, name VARCHAR(100) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_64C19C1979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registration (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_62A8A7A7979B1AD6 (company_id), INDEX IDX_62A8A7A7A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hospital (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, region_id INT NOT NULL, name VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_4282C85B979B1AD6 (company_id), INDEX IDX_4282C85B98260155 (region_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reg_value (id INT AUTO_INCREMENT NOT NULL, registration_id INT NOT NULL, form_field_id INT NOT NULL, quantity INT DEFAULT NULL, text VARCHAR(255) DEFAULT NULL, INDEX IDX_C8CB3642833D8F43 (registration_id), INDEX IDX_C8CB3642F50D82F4 (form_field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, name VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_F62F176979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE form_field ADD CONSTRAINT FK_D8B2E19B4F6E5C03 FOREIGN KEY (form_group_id) REFERENCES form_group (id)');
        $this->addSql('ALTER TABLE form_group ADD CONSTRAINT FK_EE87E00612469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE registration ADD CONSTRAINT FK_62A8A7A7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B98260155 FOREIGN KEY (region_id) REFERENCES region (id)');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB3642833D8F43 FOREIGN KEY (registration_id) REFERENCES registration (id)');
        $this->addSql('ALTER TABLE reg_value ADD CONSTRAINT FK_C8CB3642F50D82F4 FOREIGN KEY (form_field_id) REFERENCES form_field (id)');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F176979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB3642F50D82F4');
        $this->addSql('ALTER TABLE form_field DROP FOREIGN KEY FK_D8B2E19B4F6E5C03');
        $this->addSql('ALTER TABLE form_group DROP FOREIGN KEY FK_EE87E00612469DE2');
        $this->addSql('ALTER TABLE reg_value DROP FOREIGN KEY FK_C8CB3642833D8F43');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B98260155');
        $this->addSql('DROP TABLE form_field');
        $this->addSql('DROP TABLE form_group');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE registration');
        $this->addSql('DROP TABLE hospital');
        $this->addSql('DROP TABLE reg_value');
        $this->addSql('DROP TABLE region');
    }
}
