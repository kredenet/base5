<?php

namespace App\DataFixtures;

use App\Entity\Host;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class HostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $entity1 = new Host();
        $entity1->setName("Base5");
        $entity1->setDomain("base5.test");
        $manager->persist($entity1);

        $manager->flush();

        $this->setReference("host1", $entity1);
    }

}
