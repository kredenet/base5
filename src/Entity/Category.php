<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function __toString()
    {
        return $this->name;
    }

    public function __construct()
    {
        //$this->formGroups = new ArrayCollection();
        //$this->hospitals = new ArrayCollection();
        //$this->statTemplates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FormGroup[]
     */
    //public function getFormGroups(): Collection
    //{
    //    return $this->formGroups;
    //}
    //
    //public function addFormGroup(FormGroup $formGroup): self
    //{
    //    if (!$this->formGroups->contains($formGroup)) {
    //        $this->formGroups[] = $formGroup;
    //        $formGroup->setCategory($this);
    //    }
    //
    //    return $this;
    //}
    //
    //public function removeFormGroup(FormGroup $formGroup): self
    //{
    //    if ($this->formGroups->contains($formGroup)) {
    //        $this->formGroups->removeElement($formGroup);
    //        // set the owning side to null (unless already changed)
    //        if ($formGroup->getCategory() === $this) {
    //            $formGroup->setCategory(null);
    //        }
    //    }
    //
    //    return $this;
    //}

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|Hospital[]
     */
    //public function getHospitals(): Collection
    //{
    //    return $this->hospitals;
    //}
    //
    //public function addHospital(Hospital $hospital): self
    //{
    //    if (!$this->hospitals->contains($hospital)) {
    //        $this->hospitals[] = $hospital;
    //        $hospital->addCategory($this);
    //    }
    //
    //    return $this;
    //}
    //
    //public function removeHospital(Hospital $hospital): self
    //{
    //    if ($this->hospitals->contains($hospital)) {
    //        $this->hospitals->removeElement($hospital);
    //        $hospital->removeCategory($this);
    //    }
    //
    //    return $this;
    //}

    /**
     * @return Collection|StatTemplate[]
     */
    //public function getStatTemplates(): Collection
    //{
    //    return $this->statTemplates;
    //}
    //
    //public function addStatTemplate(StatTemplate $statTemplate): self
    //{
    //    if (!$this->statTemplates->contains($statTemplate)) {
    //        $this->statTemplates[] = $statTemplate;
    //        $statTemplate->setCategory($this);
    //    }
    //
    //    return $this;
    //}
    //
    //public function removeStatTemplate(StatTemplate $statTemplate): self
    //{
    //    if ($this->statTemplates->contains($statTemplate)) {
    //        $this->statTemplates->removeElement($statTemplate);
    //        // set the owning side to null (unless already changed)
    //        if ($statTemplate->getCategory() === $this) {
    //            $statTemplate->setCategory(null);
    //        }
    //    }
    //
    //    return $this;
    //}
}
