<?php /** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\host", inversedBy="companies")
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Team", mappedBy="company")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     */
    private $employees;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Content", mappedBy="company")
     */
    private $contents;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->employees = new ArrayCollection();
        //$this->statTemplates = new ArrayCollection();
        $this->contents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHost(): ?host
    {
        return $this->host;
    }

    public function setHost(?host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->setCompany($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            // set the owning side to null (unless already changed)
            if ($team->getCompany() === $this) {
                $team->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getEmployees(): Collection
    {
        return $this->employees;
    }

    public function addEmployee(User $employee): self
    {
        if (!$this->employees->contains($employee)) {
            $this->employees[] = $employee;
            $employee->setCompany($this);
        }

        return $this;
    }

    public function removeEmployee(User $employee): self
    {
        if ($this->employees->contains($employee)) {
            $this->employees->removeElement($employee);
            // set the owning side to null (unless already changed)
            if ($employee->getCompany() === $this) {
                $employee->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StatTemplate[]
     */
    //public function getStatTemplates(): Collection
    //{
    //    return $this->statTemplates;
    //}
    //
    //public function addStatTemplate(StatTemplate $statTemplate): self
    //{
    //    if (!$this->statTemplates->contains($statTemplate)) {
    //        $this->statTemplates[] = $statTemplate;
    //        $statTemplate->setCompany($this);
    //    }
    //
    //    return $this;
    //}
    //
    //public function removeStatTemplate(StatTemplate $statTemplate): self
    //{
    //    if ($this->statTemplates->contains($statTemplate)) {
    //        $this->statTemplates->removeElement($statTemplate);
    //        // set the owning side to null (unless already changed)
    //        if ($statTemplate->getCompany() === $this) {
    //            $statTemplate->setCompany(null);
    //        }
    //    }
    //
    //    return $this;
    //}

    /**
     * @return Collection|Content[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContent(Content $content): self
    {
        if (!$this->contents->contains($content)) {
            $this->contents[] = $content;
            $content->setCompany($this);
        }

        return $this;
    }

    public function removeContent(Content $content): self
    {
        if ($this->contents->contains($content)) {
            $this->contents->removeElement($content);
            // set the owning side to null (unless already changed)
            if ($content->getCompany() === $this) {
                $content->setCompany(null);
            }
        }

        return $this;
    }
}
