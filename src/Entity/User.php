<?php /** @noinspection PhpUnused */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository"),
 * @ORM\Table(
 *  uniqueConstraints={
 *      @UniqueConstraint(name="email_unique",columns={"email", "host_id"}),
 *      @UniqueConstraint(name="username_unique",columns={"username", "host_id"})
 *  }
 * )
 * @UniqueEntity(
 *  fields={"host", "email"},
 *  errorPath="email",
 *  message="Denne e-mail er allerede i brug. Prøv venligst igen."
 * )
 */
class User implements UserInterface
{
    const ROLE_HOSTMASTER = 110;
    const ROLE_SUPER_ADMIN = 100;
    const ROLE_ADMIN = 90;
    const ROLE_LEADER = 80;
    const ROLE_EMPLOYEE = 70;
    const ROLE_VOLUNTARY = 40;
    const ROLE_USER = 10;

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * This is automatic set when roles is set
     *
     * @ORM\Column(type="smallint")
     */
    private $rolesHierarchy;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $secret;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Host", inversedBy="users")
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Gedmo\Slug(fields={"firstname", "lastname"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $username;

    public const DEFAULT_PROFILE_IMAGE_PATH = "/assets/backend/images/profile.png";
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $profileImagePath = USER::DEFAULT_PROFILE_IMAGE_PATH;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Team", inversedBy="members")
     */
    private $team;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="employees")
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Also determine and sets $this->rolesHierarchy
     * @param array $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        $newLevel = 0;
        foreach ($roles as $role) {
            switch ($role) {
                case 'ROLE_HOSTMASTER':
                    $rolesHierarchy = User::ROLE_HOSTMASTER;
                    break;
                case 'ROLE_SUPER_ADMIN':
                    $rolesHierarchy = User::ROLE_SUPER_ADMIN;
                    break;
                case 'ROLE_ADMIN':
                    $rolesHierarchy = User::ROLE_ADMIN;
                    break;
                case 'ROLE_LEADER':
                    $rolesHierarchy = User::ROLE_LEADER;
                    break;
                case 'ROLE_EMPLOYEE':
                    $rolesHierarchy = User::ROLE_EMPLOYEE;
                    break;
                case 'ROLE_VOLUNTARY':
                    $rolesHierarchy = User::ROLE_VOLUNTARY;
                    break;
                case 'ROLE_USER':
                    $rolesHierarchy = User::ROLE_USER;
                    break;
                default:
                    $rolesHierarchy = 1;
                    break;
            }

            if ($rolesHierarchy > $newLevel) {
                $newLevel = $rolesHierarchy;
            }
        }

        $this->rolesHierarchy = $newLevel;

        return $this;
    }

    /**
     * @param string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->getFirstname() . " " . $this->getLastname();
    }

    public function getSecret(): ?string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): self
    {
        $this->secret = $secret;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getHost(): ?Host
    {
        return $this->host;
    }

    public function setHost(?Host $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getProfileImagePath(): ?string
    {
        return $this->profileImagePath;
    }

    public function setProfileImagePath(?string $profileImagePath): self
    {
        $this->profileImagePath = $profileImagePath;

        return $this;
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getRolesHierarchy(): ?int
    {
        return $this->rolesHierarchy;
    }

    /**
     * Also determine and sets $this->roles
     * @param int $rolesHierarchy
     * @return $this
     */
    public function setRolesHierarchy(int $rolesHierarchy): self
    {
        $this->rolesHierarchy = $rolesHierarchy;

        switch ($rolesHierarchy) {
            case User::ROLE_HOSTMASTER:
                $roles = ['ROLE_HOSTMASTER'];
                break;
            case User::ROLE_SUPER_ADMIN:
                $roles = ['ROLE_SUPER_ADMIN'];
                break;
            case User::ROLE_ADMIN:
                $roles = ['ROLE_ADMIN'];
                break;
            case User::ROLE_LEADER:
                $roles = ['ROLE_LEADER'];
                break;
            case User::ROLE_EMPLOYEE:
                $roles = ['ROLE_EMPLOYEE'];
                break;
            case User::ROLE_VOLUNTARY:
                $roles = ['ROLE_VOLUNTARY'];
                break;
            case User::ROLE_USER:
                $roles = ['ROLE_USER'];
                break;
            default:
                $roles = [];
                break;
        }
        $this->roles = $roles;

        return $this;
    }
}
