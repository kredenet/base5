<?php
namespace App\Controller\Frontend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontendPageController extends AbstractController
{
    /**
     * @Route("/", name="frontend_index")
     */
    public function index()
    {
        return $this->render("frontend/generic/frontpage.html.twig");
    }
}