<?php

namespace App\Controller\Backend\Admin;

use App\Controller\ControllerBaseTrait;
use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/employee")
 */
class EmployeeController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @Route("/", name="backend_admin_employee_index", methods={"GET"})
     * @param Request $request
     * @param UserRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function index(Request $request, UserRepository $repository, PaginatorInterface $paginator): Response
    {
        $queryBuilder = $repository->createQueryBuilder('user')
            ->where('user.company = :company')
            ->setParameter('company', $this->getUser()->getCompany())
            ->andWhere('user.rolesHierarchy >= :role')
            ->setParameter('role', User::ROLE_VOLUNTARY);

        $entities = $paginator->paginate($queryBuilder, $request->query->getInt("page", 1));

        return $this->render('backend/admin/employee/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new", name="backend_admin_employee_new", methods={"GET","POST"})
     * @param Request $request
     * @param SwissArmyKnife $swissArmyKnife
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param MailHelper $mailer
     * @return Response
     */
    public function new(Request $request, SwissArmyKnife $swissArmyKnife, UserPasswordEncoderInterface $passwordEncoder, MailHelper $mailer): Response
    {

        $entity = new User();
        $entity
            ->setHost($this->getUser()->getHost())
            ->setCompany($this->getUser()->getCompany());

        $form = $this->createForm(UserType::class, $entity);
        $form->add('Gem', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**
             * password
             */
            $password = $swissArmyKnife->generatePassword();
            $entity->setPassword($passwordEncoder->encodePassword($entity, $password));
            $entity->setSecret($swissArmyKnife->makeSecret($password));

            if (!$entity->getUsername()) {
                $entity->setUsername($entity->getEmail());
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $mailData = [];
            $mailData["content"] = $this->renderView("backend/mail/welcome.html.twig", [
                "host"=> $this->getUser()->getHost()->getDomain(),
                "user" => $entity,
                "password" => $password
            ]);
            $mailData["firstname"] = $entity->getFirstname();
            $mailer->sendMail($entity, false, $swissArmyKnife->t("Velkommen", "mail"), $mailData);

            $this->addFlash("success", "Medarbejder oprettet! Der er udsendt velkomstmail med autogenereret adgangskode.");

            return $this->redirectToRoute('backend_admin_employee_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Opret ny bruger',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_employee_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $entity
     * @return Response
     */
    public function edit(Request $request, User $entity): Response
    {
        $form = $this->createForm(UserType::class, $entity);

        $form->add('Gem', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_admin_employee_index');
        }

        return $this->render('backend/base_form.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'title' => 'Rediger bruger',
        ]);
    }

    /**
     * @Route("/{id}", name="backend_admin_employee_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $entity
     * @return Response
     */
    public function delete(Request $request, User $entity): Response
    {
        if ($this->isCsrfTokenValid('delete' . $entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();
        }

        return $this->redirectToRoute('backend_admin_employee_index');
    }
}
