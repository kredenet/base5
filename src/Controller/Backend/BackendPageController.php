<?php
namespace App\Controller\Backend;

use App\Controller\ControllerBaseTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */

class BackendPageController extends AbstractController
{
    use ControllerBaseTrait;

    /**
     * @Route("/", name="base_backend_admin_home")
     */
    public function index()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        //$statQuantity = $em->getRepository('App:Registration')->createQueryBuilder('registration')
        //    ->select('count(registration.id) as quantity')
        //    ->where('registration.company = :company')
        //    ->setParameter('company', $this->getUser()->getCompany())
        //    ->andWhere('registration.createdAt > :date')
        //    ->setParameter('date', new \DateTime('today - 30 days'))
        //    ->getQuery()->getSingleScalarResult();
        //
        return $this->render("backend/admin/dashboard.html.twig", [
            //'statQuantity' => $statQuantity,
        ]);
    }
    /**
     * @Route("/test1", name="base_backend_test1")
     */
    public function test1()
    {
        return $this->render("backend/admin/dashboard.html.twig");
    }
    /**
     * @Route("/test2", name="base_backend_test2")
     */
    public function test2()
    {
        return $this->render("backend/admin/dashboard.html.twig");
    }
    /**
     * @Route("/test2-1", name="base_backend_test21")
     */
    public function test21()
    {
        return $this->render("backend/admin/dashboard.html.twig");
    }
    /**
     * @Route("/test2-2", name="base_backend_test22")
     */
    public function test22()
    {
        return $this->render("backend/admin/dashboard.html.twig");
    }
    /**
     * @Route("/test3", name="base_backend_test3")
     */
    public function test3()
    {
        return $this->render("backend/admin/dashboard.html.twig");
    }

}
