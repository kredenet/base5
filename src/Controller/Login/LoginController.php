<?php

namespace App\Controller\Login;

use App\Entity\Content;
use App\Entity\User;
use App\Form\UserNewPassword;
use App\Form\UserResetPassword;
use App\Service\HostHelper;
use App\Service\MailHelper;
use App\Service\SwissArmyKnife;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController
 * @package App\Controller\Login
 * @property UserPasswordEncoderInterface passwordEncoder
 * @property ObjectManager em
 * @property HostHelper hostHelper
 * @property MailHelper mailHelper
 * @property SwissArmyKnife swissArmyKnife
 */
class LoginController extends AbstractController
{
    private $host;

    /**
     * AdminController constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param EntityManagerInterface $entityManager
     * @param MailHelper $mailer
     * @param HostHelper $hostHelper
     * @param SwissArmyKnife $swissArmyKnife
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EntityManagerInterface $entityManager,
        MailHelper $mailer,
        HostHelper $hostHelper,
        SwissArmyKnife $swissArmyKnife
        )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $entityManager;
        $this->mailer = $mailer;
        $this->hostHelper = $hostHelper;
        $this->host = $this->hostHelper->getHost();
        $this->swissArmyKnife = $swissArmyKnife;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //    $this->redirectToRoute("target_path");
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $contentHeadline = $this->getDoctrine()->getRepository(Content::class)->findOneBy(["name" => "login_intro_headline"]);
        $content = $this->getDoctrine()->getRepository(Content::class)->findOneBy(["name" => "login_intro"]);

        return $this->render("login/login.html.twig", [
            "contentHeadline" => $contentHeadline,
            "content" => $content,
            "last_username" => $lastUsername,
            "error" => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     * @throws \Exception
     */
    public function logout()
    {
        throw new \Exception("This method can be blank - it will be intercepted by the logout key on your firewall");
    }

    /**
     * @Route("/login/forgot-password", name="login_forgot_password")
     * @param Request $request
     * @return Response
     */
    public function loginForgotPassword(Request $request)
    {
        $form = $this->createForm(UserResetPassword::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();

            /** @var User $user */
            $user = $this->em->getRepository("App:User")->findOneBy(["email" => $formData->getEmail()]);

            if ($user && $user->getEmail()) {
                $mailData = [];
                $mailData["content"] = $this->renderView("backend/mail/reset_password.html.twig", ["host"=> $this->host->getDomain(), "user" => $user]);
                $mailData["firstname"] = $user->getFirstname();
                $this->mailer->sendMail($user, false, $this->swissArmyKnife->t("Nulstilling af adgangskode"), $mailData);

                $this->addFlash("success", "Mail for nulstilling af adgangskode er afsendt.");
                return $this->redirectToRoute("base_backend_admin");
            }
            else {
                $this->addFlash("error", "Kunne ikke finde e-mail ".$formData->getEmail().". Prøv venligst igen.");
            }
        }

        $content = $this->getDoctrine()->getRepository(Content::class)->findOneBy(["name" => "forgot_password"]);

        return $this->render("login/forgot_password.html.twig", [
            "content" => $content,
            "request" => $request,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/login/new-password/{secret}", name="login_new_password", requirements={"secret"=".+"})
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function loginNewPassword(User $user, Request $request)
    {
        $form = $this->createForm(UserNewPassword::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $formData = $form->getData();

            //encode password
            $password = $formData->getPassword();
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setSecret($this->swissArmyKnife->makeSecret($password));

            $this->em->persist($user);
            $this->em->flush();

            if ($user->getEmail()) {
                $this->addFlash("success", "Din nye adgangskode er nu gemt.");
            }

            return $this->redirectToRoute("base_backend_admin");
        }

        return $this->render("login/new_password.html.twig", [
            "request" => $request,
            "form" => $form->createView()
        ]);
    }

}
