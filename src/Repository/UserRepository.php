<?php

namespace App\Repository;

use App\Entity\Host;
use App\Entity\User;
use App\Service\HostHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method User|null find($id, $lockMode = NULL, $lockVersion = NULL)
 * @method User|null findOneBy(array $criteria, array $orderBy = NULL)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
 */
class UserRepository extends ServiceEntityRepository
{

    /** @var HostHelper $hostHelper */
    private $hostHelper;

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     * @param HostHelper $hostHelper
     */
    public function __construct(ManagerRegistry $registry, HostHelper $hostHelper)
    {
        parent::__construct($registry, User::class);
        $this->hostHelper = $hostHelper;
    }

    /**
     * @param $q
     * @return Query
     */
    public function getBySearch($q)
    {
        $qFields = ["email", "firstname", "lastname"];
        $aWhereString = "entity." . implode(" LIKE :q OR entity.", $qFields) . " LIKE :q";

        return $this->createQueryBuilder("entity")

            //host
            ->innerJoin("entity.host", "host")
            ->where("host = :host")
            ->setParameter("host", $this->hostHelper->getHost())

            //role
            ->andWhere("entity.roles LIKE :role")
            ->setParameter("role", "%ROLE_ADMIN%")

            //q
            ->andWhere($aWhereString)
            ->setParameter("q", "%" . $q . "%")
            ->orderBy("entity.id", "ASC")
            ->getQuery();
    }

    /**
     * @param Host|null $host
     * @return Query
     */
    public function getAllAdminsByHost(Host $host = NULL)
    {
        $builder = $this->createQueryBuilder("entity");

        //host
        $builder
            ->innerJoin("entity.host", "host")
            ->where("host = :host");

        if ($host) {
            $builder->setParameter("host", $host);
        } else {
            $builder->setParameter("host", $this->hostHelper->getHost());
        }

        //role
        //$builder
        //    ->andWhere("entity.roles LIKE :role")
        //   ->setParameter("role", "%ROLE_ADMIN%")
        //   ->orderBy("entity.id", "ASC");

        return $builder->getQuery();
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function getAllAdmins()
    {
        return $this->createQueryBuilder("entity")
            ->where("entity.roles LIKE :role")
            ->setParameter("role", "%ROLE_ADMIN%")
            ->orderBy("entity.id", "ASC")
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder("u")
            ->andWhere("u.exampleField = :val")
            ->setParameter("val", $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
