<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

class SwissArmyKnife extends AbstractController
{
    /** @var RequestStack $request */
    private $request;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var EntityManagerInterface $em */
    private $translator;

    public function __construct(RequestStack $request, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->request = $request;
        $this->em = $em;
        $this->translator = $translator;
    }

    /**
     * @param $string
     * @param string $domain
     * @return false|string
     */
    public function t($string, $domain = "backend")
    {
        return $this->translator->trans($string, [], $domain);
    }

    /**
     * @param int $lenght
     * @return false|string
     */
    public function generatePassword($lenght = 8)
    {
        $password = substr(str_shuffle("ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789!&#*"), 0, $lenght);
        return $password;
    }

    /**
     * @param bool $seed
     * @param bool $md5
     * @return false|string
     */
    public function makeSecret($seed = false, $md5 = true)
    {
        if (!$seed) {
            $seed = $this->generatePassword(10);
        }
        $secret = password_hash($seed, PASSWORD_DEFAULT);
        if ($md5) {
            $secret = md5($secret);
        }
        return $secret;
    }

    /**
     * @param User $entity
     * @return false|string
     */
    public function getProfileImagePath(User $entity)
    {
        $possibleGravatar = "https://s.gravatar.com/avatar/".md5($entity->getEmail())."?s=254&r=PG&d=404";
        if (@file_get_contents($possibleGravatar)) {
            $profileImagePath = $possibleGravatar;
        }
        else {
            $profileImagePath = User::DEFAULT_PROFILE_IMAGE_PATH;
        }

        return $profileImagePath;
    }
}

