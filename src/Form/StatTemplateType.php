<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\FormGroup;
use App\Entity\StatTemplate;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var StatTemplate $entity */
        $entity = $builder->getData();

        $company = $entity->getCompany();

        if ($entity->getCategory()) {
            foreach ($entity->getCategory()->getFormGroups() as $formGroup) {
                $this->choices($formGroup);
            }
        }

        $builder
            ->add('name', null, [
                'label' => 'Navn',
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) use ($company) {
                    return $er->createQueryBuilder('category')
                        ->where('category.company = :company')
                        ->setParameter('company', $company)
                        ->orderBy('category.id', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => false,
                'required' => true,
                'label' => 'Kategori',
                'placeholder' => 'Vælg kategori',
            ])
            ->add('columns', EntityType::class, [
                'class' => FormGroup::class,
//                'query_builder' => function (EntityRepository $er) use ($company) {
//                    return $er->createQueryBuilder('category')
//                        ->orderBy('category.id', 'ASC');
//                },
                'choices' => $this->choices,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => false,
                'required' => false,
                'label' => 'Kolonner',
                'placeholder' => 'Vælg kolonner',
                'attr' => [
                    'style' => 'min-height: 250px;'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StatTemplate::class,
            "translation_domain" => "backend",
        ]);
    }

    private $choices = [];

    private function choices(FormGroup $formGroup)
    {
        if ($formGroup->getType() === FormGroup::TYPE_GROUP) {
            foreach ($formGroup->getLowerFormGroups() as $lowerFormGroup) {
                $this->choices($lowerFormGroup);
            }
        } else {
            $this->choices[] = $formGroup;
        }

    }
}
