<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserType extends AbstractType
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $entity */
//        $entity = $builder->getData();

//        $company = $entity->getCompany();


        $builder
            ->add('firstname', null, [
                'label' => 'Fornavn',
            ])
            ->add('lastname', null, [
                'label' => 'Efternavn',
            ])
            ->add('username', null, [
                'label' => 'Brugernavn',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
            ]);
//            ->add('team', EntityType::class, [
//                'label' => 'Team',
//                'class' => Team::class,
//                'query_builder' => function (EntityRepository $er) use ($company) {
//                    return $er->createQueryBuilder('team')
//                        ->where('team.company = :company')
//                        ->setParameter('company', $company)
//                        ->orderBy('team.name', 'asc');
//                },
//                'choice_label' => 'name',
//                'required' => false,
//                'placeholder' => 'Vælg',
//                'attr' => [
//                    'class' => 'selectpicker',
//                    'data-live-search' => true,
//                ],
//            ]);


        /** @var User $entity */
        $entity = $builder->getData();

        if (!$entity->getId()) {
            $choices = [];

            $role = $this->security->getUser()->getRolesHierarchy();

            if ($role >= 90) {
                $choices['Frivillig'] = User::ROLE_VOLUNTARY;
                $choices['Admin'] = User::ROLE_ADMIN;
            }

            if ($role >= 100) {
                $choices['Super admin'] = User::ROLE_SUPER_ADMIN;
            }

            $builder
                ->add('rolesHierarchy', ChoiceType::class, [
                    'label' => 'Rolle',
                    'choices' => $choices,
                    'placeholder' => 'Vælg rolle'
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'backend',
        ]);
    }
}
