<?php

namespace App\Form;

use App\Entity\Team;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Team $entity */
        $entity = $builder->getData();

        $company = $entity->getCompany();

        $builder
            ->add('name', null, [
                'label' => 'Navn',
            ])
            ->add('leader', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (EntityRepository $er) use ($company) {
                    return $er->createQueryBuilder('user')
                        ->where('user.company = :company')
                        ->setParameter('company', $company)
                        ->andWhere('user.rolesHierarchy >= :role')
                        ->setParameter('role', User::ROLE_LEADER)
                        ->orderBy('user.firstname', 'asc')
                        ->orderBy('user.lastname', 'asc');
                },
                'choice_label' => 'name',
                'required' => false,
                'placeholder' => 'Vælg',
                'attr' => [
                    'class' => 'selectpicker',
                    'data-live-search' => true,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
            "translation_domain" => "backend",
        ]);
    }
}
