<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Hospital;
use App\Entity\Registration;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var Registration $entity */
        $entity = $builder->getData();

        $company = $entity->getCompany();

        $builder
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'query_builder' => function (EntityRepository $er) use ($company) {
                    return $er->createQueryBuilder('category')
                        ->where('category.company = :company')
                        ->setParameter('company', $company)
                        ->orderBy('category.id', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => false,
                'expanded' => true,
                'required' => true,
                'label' => 'Vælg kategori'
            ]);

        $builder
            ->add('hospital', EntityType::class, [
                'class' => Hospital::class,
                'query_builder' => function (EntityRepository $er) use ($company) {
                    return $er->createQueryBuilder('hospital')
                        ->where('hospital.company = :company')
                        ->setParameter('company', $company)
                        ->innerJoin('hospital.region', 'region')
                        ->orderBy('region.id', 'ASC')
                        ->addOrderBy('hospital.id', 'ASC');
                },
                'choice_label' => 'name',
                'multiple' => false,
                // todo google group by og expanded fieldset / div
                'expanded' => true,
                'required' => true,
                'group_by' => 'region.name',
                'placeholder' => 'Vælg hospital',
            ]);

//        $builder->add('regValues');

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Registration::class,
            "translation_domain" => "backend",
            'csrf_protection' => false,
        ]);
    }
}
