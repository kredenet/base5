<?php

namespace App\Form;

use App\Entity\FormGroup;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => false,
                'attr' => [
                    'aria-label' => 'Spørgsmål',
                    'placeholder' => 'Spørgsmål',
                ],
            ])
            ->add('type', ChoiceType::class, [
                'placeholder' => 'Vælg type',
                'label' => false,
                'required' => true,
                'choices' => FormGroup::getTypes(),
                'attr' => [
                    'class' => 'choiceFlipper',
                    'aria-label' => 'Vælg type',
                ],
            ])
            ->add('choices', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'choiceFlipperChoice ' . FormGroup::TYPE_CHOICE . 'Choice ' . FormGroup::TYPE_MULTIPLE_CHOICE . 'Choice',
                    'style' => 'min-height: 200px;'
                ],
                'label' => 'Valgmuligheder'
            ]);

        $transformer = new CallbackTransformer(
            function ($serviceCodesAsArray) {
                if (!is_array($serviceCodesAsArray)) {
                    return '';
                }

                $string = join("\n", $serviceCodesAsArray);

                $string = str_replace(";", ",", $string);

                return $string;
            },
            function ($serviceCodesAsString) {
                $serviceCodesAsString = str_replace(",", ";", $serviceCodesAsString);

                $serviceCodesAsArray = preg_split('/[\n]+/i', $serviceCodesAsString, null, PREG_SPLIT_NO_EMPTY);

                return array_map('trim', $serviceCodesAsArray);
            }
        );

        $builder->get('choices')->addModelTransformer($transformer);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormGroup::class,
            "translation_domain" => "backend",
        ]);
    }
}
