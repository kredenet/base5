


## sortering af bruger uden discriminator

### nuværende løsning
    SELECT * FROM `user` where JSON_SEARCH(roles, 'one', "ROLE_ADMIN") is not null

Det vil ikke virke med nedarvning

### Løsning med level

    integer roleLevel

    110 ROLE_HOSTMASTER
    100 ROLE_SUPER_ADMIN
    90  ROLE_ADMIN
    80  ROLE_LEADER
    70  ROLE_EMPLOYEE
    40  ROLE_VOLUNTARY
    10  ROLE_USER
    
Søgning

    SELECT * FROM `user` where roleLevel >= '90'

roles kan vi beholde til hvis der er tilføjelser der ikke er i hieraki

