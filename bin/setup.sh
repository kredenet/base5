#!/usr/bin/env bash

#config

name=base5

host=$name.test
database=base5
projectPath=/var/www/$name/public
webroot=$projectPath

#don't edit below

function make_vhost
{
cat <<- _EOF_
<VirtualHost *:80>
        ServerName $host
        ServerAlias *.$host

        ServerAdmin webmaster@$host

        DocumentRoot $webroot

        <Directory "$webroot">
                AllowOverride All
                Options All -indexes

#                AuthType Basic
#                AuthName "Secure Content"
#                AuthBasicProvider file
#                AuthUserFile /etc/apache2/passwords
#                Require valid-user
        </Directory>
</VirtualHost>

<VirtualHost *:443>
        ServerName $host
        ServerAlias *.$host

        ServerAdmin webmaster@$host

        KeepAlive On
        SSLEngine on
        SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP

        SSLCertificateFile      /etc/ssl/certs/$host.crt
        SSLCertificateKeyFile   /etc/ssl/private/$host.key

        DocumentRoot $webroot

        <Directory "$webroot">
                AllowOverride All
                Options All -indexes

#                AuthType Basic
#                AuthName "Secure Content"
#                AuthBasicProvider file
#                AuthUserFile /etc/apache2/passwords
#                Require valid-user
        </Directory>
</VirtualHost>
_EOF_
}

make_vhost > /etc/apache2/sites-enabled/$host.conf

#certificate
/usr/bin/openssl req -new -newkey rsa:4096 -days 1500 -nodes -x509 -subj '/C=DK/ST=Denial/L=Hadsten/O=Domain/CN=$host' -keyout /etc/ssl/private/$host.key  -out /etc/ssl/certs/$host.crt

#add to hosts
echo "127.0.0.1 $host" >> /etc/hosts

#database setup
mysql -uweb -pweb -e "create database if not exists $database"

#restart services
service apache2 graceful

echo "Done"
exit
