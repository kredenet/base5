$("input.select-all-checkboxes").click(function (event) {
    let allCheckboxes = $(this).closest("table").find("input[type='checkbox']");
    if ($(this).is(":checked")) {
        allCheckboxes.prop('checked', true);
    } else {
        allCheckboxes.prop('checked', false);
    }
});
$("input[type='checkbox']").click(function (event) {
    if (event.shiftKey) {
        let item = $(this);
        let hitfirstChecked = false;
        $("input.entity-checkbox").each(function () {
            if ($(this).is(":checked")) {
                hitfirstChecked = true;
            }
            if (hitfirstChecked) {
                $(this).prop('checked', true);
            }
            if (item.is($(this))) {
                return false;
            }
        });
    }
});
$("input[type='checkbox']").change(function (event) {
    if ($("input.entity-checkbox:checked").length > 0) {
        $(".delete-entities").fadeIn();
    } else {
        $(".delete-entities").fadeOut();
    }
});
$(".delete-entities").click(function (event) {

    if (window.confirm(confirmMsg)) {
        let ids = [];
        let checkedInputs = $("input.entity-checkbox:checked");
        checkedInputs.each(function () {
            ids.push($(this).attr("data-id"));
        });
        $.ajax({
            method: "POST",
            json: true,
            url: pathDeleteEntities,
            data: {
                ids: ids
            },
            success: function( response ) {
                if(response.reload === false) {
                    checkedInputs.each(function () {
                        $(this).closest("tr").fadeOut();
                        $(".delete-entities").fadeOut();
                    });
                }
            }
        })
    }
    else {
        return false;
    }
});
$(".show-actions").click(function (event) {
    event.preventDefault();
    let _this = $(this);
    $(this).closest("td").animate({height:'100%'},200).promise().then(function(){
        _this.hide();
        _this.closest(".actions").find(".actions-wrapper").fadeIn();
    });
});
